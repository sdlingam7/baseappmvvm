package com.application.core.base

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.Window
import android.widget.RelativeLayout
import android.widget.TextView
import com.application.core.R


object CustomActionWindow {

    fun showSingleButtonWindow(
        context: Context,
        strMessage: String,
        strYesName: String,
        onActionResult: CustomActionWindow.OnActionResult
    ) {
        val alertDialog = Dialog(context)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.custom_single_button_alert)
        alertDialog.setCancelable(false)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val relativeYes = alertDialog.findViewById(R.id.relativeYes) as RelativeLayout
        val txtMessage = alertDialog.findViewById(R.id.txtMessage) as TextView
        val txtYes = alertDialog.findViewById(R.id.txtYes) as TextView

        txtMessage.setText(strMessage)
        txtYes.setText(strYesName)

        relativeYes.setOnClickListener(View.OnClickListener {
            alertDialog.dismiss()
            onActionResult.onClickYes()
        })
        alertDialog.show()
    }


    fun showDoubleButtonWindow(
        context: Context,
        strMessage: String,
        strYesName: String,
        strNoName: String,
        onActionResult: CustomActionWindow.OnActionResult
    ) {
        val alertDialog = Dialog(context)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.custom_double_button_alert)
        alertDialog.setCancelable(false)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val relativeYes = alertDialog.findViewById(R.id.relativeYes) as RelativeLayout
        val relativeNo = alertDialog.findViewById(R.id.relativeNo) as RelativeLayout
        val txtMessage = alertDialog.findViewById(R.id.txtMessage) as TextView
        val txtYes = alertDialog.findViewById(R.id.txtYes) as TextView
        val txtNo = alertDialog.findViewById(R.id.txtNo) as TextView

        txtMessage.setText(strMessage)
        txtYes.setText(strYesName)
        txtNo.setText(strNoName)

        relativeYes.setOnClickListener(View.OnClickListener {
            alertDialog.dismiss()
            onActionResult.onClickYes()
        })

        relativeNo.setOnClickListener(View.OnClickListener {
            alertDialog.dismiss()
            onActionResult.onClickNo()
        })
        alertDialog.show()
    }

    interface OnActionResult {
        fun onClickYes()

        fun onClickNo()
    }
}
