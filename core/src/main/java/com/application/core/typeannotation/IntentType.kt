/*
 * Copyright 2019 PLAY DATE. All rights reserved.
 *
 * Dissemination of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from PLAY DATE.
 *
 * Created by Dharmalingam Sekar
 * Updated on 19/6/19 5:09 PM
 *
 */

package com.application.core.typeannotation

import androidx.annotation.IntDef
import com.application.core.typeannotation.IntentType.Companion.CLEAR_TASK
import com.application.core.typeannotation.IntentType.Companion.SINGLE_TOP
import com.application.core.typeannotation.IntentType.Companion.DEFAULT

@IntDef(
    SINGLE_TOP,
    CLEAR_TASK,
    DEFAULT
)
annotation class IntentType {
    companion object {
        const val SINGLE_TOP = 0
        const val CLEAR_TASK = 1
        const val DEFAULT = 2
    }
}

