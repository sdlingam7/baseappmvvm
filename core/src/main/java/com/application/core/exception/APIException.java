/*
 * Copyright 2019 PLAY DATE. All rights reserved.
 *
 * Dissemination of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from PLAY DATE.
 *
 * Created by Dharmalingam Sekar
 * Updated on 19/6/19 5:09 PM
 *
 */

package com.application.core.exception;

import java.io.IOException;

public class APIException extends IOException {
    private String message;

    public APIException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
