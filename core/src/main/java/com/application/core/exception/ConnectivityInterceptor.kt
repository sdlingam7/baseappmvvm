/*
 * Copyright 2019 PLAY DATE. All rights reserved.
 *
 * Dissemination of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from PLAY DATE.
 *
 * Created by Dharmalingam Sekar
 * Updated on 19/6/19 5:09 PM
 *
 */

package com.application.core.exception

import android.content.Context
import com.application.core.utils.ErrorUtils
import com.application.core.utils.extension.isOnline
import okhttp3.Interceptor
import okhttp3.Response

class ConnectivityInterceptor(private val mContext: Context) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        if (!mContext.isOnline()) {
            throw NoConnectivityException()
        }

        val originalRequest = chain.request()
        val response = chain.proceed(originalRequest)


        when (response.code()) {

            500 -> throw APIException(ErrorUtils.ERROR_SERVER)

            401 -> throw APIException(ErrorUtils.ERROR_SERVER)

            400 -> throw APIException(ErrorUtils.ERROR_BAD_REQUEST)
        }

        return response
    }

}
