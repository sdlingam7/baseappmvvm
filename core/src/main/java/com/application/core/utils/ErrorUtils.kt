/*
 * Copyright 2019 PLAY DATE. All rights reserved.
 *
 * Dissemination of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from PLAY DATE.
 *
 * Created by Dharmalingam Sekar
 * Updated on 19/6/19 5:09 PM
 *
 */

package com.application.core.utils

object ErrorUtils {
    const val ERROR_SERVER = "Internal Server Error"
    const val ERROR_BAD_REQUEST = "Bad Request"
    const val ERROR = "Some thing sent wrong, Please try again later"
    const val NETWORK_ERROR = "Check your internet connection"
}
