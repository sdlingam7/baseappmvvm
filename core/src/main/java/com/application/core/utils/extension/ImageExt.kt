/*
 * Copyright 2019 PLAY DATE. All rights reserved.
 *
 * Dissemination of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from PLAY DATE.
 *
 * Created by Dharmalingam Sekar
 * Updated on 19/6/19 5:09 PM
 *
 */

package com.application.core.utils.extension

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.os.Environment
import android.provider.MediaStore
import android.widget.ImageView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

fun ImageView.setImage(url: String, placeHolder: Int) {
    if (url.isNotEmpty()) {
        Picasso.get()
                .load(url)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .placeholder(placeHolder)
                .error(placeHolder)
                .into(this)
    }
}

fun ImageView.setImage(url: String) {
    if (url.isNotEmpty()) {
        val circularProgressDrawable = CircularProgressDrawable(context)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()

        Picasso.get()
                .load(url)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .placeholder(circularProgressDrawable)
                .error(circularProgressDrawable)
                .into(this)
    }
}

fun ImageView.removeAlpha(){
    this.alpha = 1f
}

fun Bitmap.getResizedBitmap(): Bitmap {
    val width = this.width
    val height = this.height
    val newWidth = 200
    val newHeight = 200
    val scaleWidth = newWidth.toFloat() / width
    val scaleHeight = newHeight.toFloat() / height
    // CREATE A MATRIX FOR THE MANIPULATION
    val matrix = Matrix()
    // RESIZE THE BIT MAP
    matrix.postScale(scaleWidth, scaleHeight)

    // "RECREATE" THE NEW BITMAP
    return Bitmap.createBitmap(
            this, 0, 0, width, height, matrix, false)
}

fun Intent.getSelectedBitmap(): Bitmap? {
    val extras = this.extras
    return extras?.getParcelable("data")
}

fun Intent.getBitmapFromGallery(context: Context): Bitmap? {
    var selectedBitmap: Bitmap? = null
    val contentURI = this.data
    try {
        val bitmap = MediaStore.Images.Media.getBitmap(context.contentResolver, contentURI)
        selectedBitmap = bitmap.getResizedBitmap()
    } catch (e: IOException) {
        e.printStackTrace()
    }

    return selectedBitmap
}

fun Bitmap.saveImage(filename: String, imageDirectory: String): String {
    val bytes = ByteArrayOutputStream()
    this.compress(Bitmap.CompressFormat.PNG, 90, bytes)
    val directory = File(Environment.getExternalStorageDirectory().toString() + imageDirectory)
    if (!directory.exists())
        directory.mkdirs()

    val file = File(directory, filename)
    // Encode the file as a PNG image.
    val outStream: FileOutputStream
    try {
        outStream = FileOutputStream(file)
        this.compress(Bitmap.CompressFormat.PNG, 100, outStream)
        outStream.flush()
        outStream.close()
        return file.absolutePath
    } catch (e: IOException) {
        e.printStackTrace()
    }

    return file.absolutePath
}

fun Bitmap.toByteArray
        (): ByteArray {
    val stream = ByteArrayOutputStream()
    this.compress(Bitmap.CompressFormat.PNG, 100, stream)
    return stream.toByteArray()
}

fun ByteArray.toBitmap(): Bitmap {
    return BitmapFactory.decodeByteArray(this, 0, this.size)
}