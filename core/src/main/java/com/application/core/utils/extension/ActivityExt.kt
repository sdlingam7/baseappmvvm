/*
 * Copyright 2019 PLAY DATE. All rights reserved.
 *
 * Dissemination of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from PLAY DATE.
 *
 * Created by Dharmalingam Sekar
 * Updated on 19/6/19 5:09 PM
 *
 */

package com.application.core.utils.extension

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.util.DisplayMetrics
import android.view.WindowManager
import android.widget.Toast
import com.application.core.typeannotation.IntentType
import com.application.core.utils.Constants
import java.util.*

fun Activity.getDisplayMetrics(): DisplayMetrics = resources.displayMetrics

fun Activity.getScreenHeight(): Int {
    return getDisplayMetrics().heightPixels
}

fun Activity.screenWidth(): Int {
    return getDisplayMetrics().widthPixels
}

fun Activity.hideKeyBoard() {
    this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
}

fun Activity.toast(message: CharSequence?, duration: Int = Toast.LENGTH_SHORT) =
        Toast.makeText(this, message, duration).show()

fun Activity.getExtraString(key: String): String {
    return if (intent.hasExtra(key)) {
        intent.getStringExtra(key)
    } else ""
}

fun Activity.getExtraInt(key: String): Int {
    return if (intent.hasExtra(key)) {
        intent.getIntExtra(key, 0)
    } else 0
}

fun Activity.closeActivity() {
    finish()
}

fun Activity.startAppActivity(c: Class<*>) {
    val intent = getIntent(c, IntentType.DEFAULT)
    launchIntent(intent)
}

fun Activity.startAppActivityClear(c: Class<*>) {
    val intent = getIntent(c, IntentType.CLEAR_TASK)
    launchIntent(intent)
    finishAffinity()
}

fun Activity.startAppActivityFinish(c: Class<*>) {
    startAppActivity(c)
    closeActivity()
}

fun Activity.startAppActivityForResult(c: Class<*>, requestCode: Int) {
    val intent = getIntent(c, IntentType.DEFAULT)
    startActivityForResult(intent, requestCode)
}

fun Activity.returnActivityWithResult(map: HashMap<String, String>) {
    val returnIntent = Intent()
    for ((key, value) in map) {
        returnIntent.putExtra(key, value)
    }
    setResult(Activity.RESULT_OK, returnIntent)
    closeActivity()
}

fun Activity.startAppActivityPutExtra(c: Class<*>, map: HashMap<String, String>) {
    val intent = getIntent(c, IntentType.DEFAULT)
    for ((key, value) in map) {
        intent.putExtra(key, value)
    }
    launchIntent(intent)
}

fun Activity.launchIntent(intent: Intent) {
    startActivity(intent)
}

fun Activity.getIntent(c: Class<*>, @IntentType intentType: Int): Intent {
    val intent = Intent(this, c)
    when (intentType) {

        IntentType.SINGLE_TOP -> intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or
                Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or
                Intent.FLAG_ACTIVITY_SINGLE_TOP or
                Intent.FLAG_ACTIVITY_CLEAR_TOP

        IntentType.CLEAR_TASK -> {
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or
                    Intent.FLAG_ACTIVITY_CLEAR_TASK
        }

        IntentType.DEFAULT -> {
        }
    }
    return intent
}

inline fun Activity.alertDialog(body: AlertDialog.Builder.() -> AlertDialog.Builder): AlertDialog {
    return AlertDialog.Builder(this)
            .body()
            .show()
}

fun Activity.openCamera() {
    val intent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
    startActivityForResult(intent, Constants.IntentResult.CAMERA)
}

fun Activity.openGallery() {
    val galleryIntent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
    startActivityForResult(galleryIntent, Constants.IntentResult.GALLERY)
}

