/*
 * Copyright 2019 PLAY DATE. All rights reserved.
 *
 * Dissemination of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from PLAY DATE.
 *
 * Created by Dharmalingam Sekar
 * Updated on 19/6/19 5:09 PM
 *
 */

package com.application.core.utils.extension

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.text.style.SuperscriptSpan
import android.util.Patterns

fun String.isValidMobileNumber(): Boolean {
    return Patterns.PHONE.matcher(this).matches()
}

fun String.isValidEmail(): Boolean {
    return Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

private const val MIN_CHAR = 8
private const val MAX_CHAR = 16
fun String.isExceedsMinMax(min: Int = MIN_CHAR): Boolean {
    return this.length < min || this.length > MAX_CHAR
}

fun String.formatToAmount(): String {
    return String.format("%,d", this.toDouble())
}

fun String.notEquals(other: String?, ignoreCase: Boolean = false): Boolean {
    return !this.equals(other, ignoreCase)
}

/**
 * String Styling using Spannable
 */

fun SpannableStringBuilder.updateSpan(span: Any, updateText: String) {
    setSpan(span,
            this.indexOf(updateText),
            this.indexOf(updateText) + updateText.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

}

fun String.getColorSpan(updateText: String, color: Int): SpannableStringBuilder {
    val spannableStringBuilder = SpannableStringBuilder(this)

    val foregroundColorSpan = ForegroundColorSpan(color)
    spannableStringBuilder.updateSpan(foregroundColorSpan, updateText)

    return spannableStringBuilder
}

fun String.getRelativeSizeSpan(updateText: String, proportion: Float = 2f): SpannableStringBuilder {
    val spannableStringBuilder = SpannableStringBuilder(this)

    val relativeSizeSpan = RelativeSizeSpan(proportion)
    spannableStringBuilder.updateSpan(relativeSizeSpan, updateText)

    return spannableStringBuilder
}

fun String.getSizeAndColorSpan(updateText: String, color: Int, proportion: Float = 1f): SpannableStringBuilder {
    val spannableStringBuilder = SpannableStringBuilder(this)

    val relativeSizeSpan = RelativeSizeSpan(proportion)
    spannableStringBuilder.updateSpan(relativeSizeSpan, updateText)

    val foregroundColorSpan = ForegroundColorSpan(color)
    spannableStringBuilder.updateSpan(foregroundColorSpan, updateText)

    return spannableStringBuilder
}

fun String.getBoldSpan(updateText: String, color: Int, proportion: Float = 1.2f): SpannableStringBuilder {

    val spannableStringBuilder = SpannableStringBuilder(this)

    val boldSpan = StyleSpan(android.graphics.Typeface.BOLD)
    spannableStringBuilder.updateSpan(boldSpan, updateText)

    val relativeSizeSpan = RelativeSizeSpan(proportion)
    spannableStringBuilder.updateSpan(relativeSizeSpan, updateText)

    val foregroundColorSpan = ForegroundColorSpan(color)
    spannableStringBuilder.updateSpan(foregroundColorSpan, updateText)

    return spannableStringBuilder

}

fun String.getSuperScriptSpan(textToSmall: String, color: Int): SpannableStringBuilder {
    val spannableStringBuilder = SpannableStringBuilder(this)

    val superscriptSpan = SuperscriptSpan()
    spannableStringBuilder.updateSpan(superscriptSpan, textToSmall)

    val relativeSizeSpan = RelativeSizeSpan(2f)
    spannableStringBuilder.updateSpan(relativeSizeSpan, textToSmall)

    val foregroundColorSpan = ForegroundColorSpan(color)
    spannableStringBuilder.updateSpan(foregroundColorSpan, textToSmall)

    return spannableStringBuilder
}