/*
 * Copyright 2019 PLAY DATE. All rights reserved.
 *
 * Dissemination of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from PLAY DATE.
 *
 * Created by Dharmalingam Sekar
 * Updated on 19/6/19 5:09 PM
 *
 */

package com.application.core.utils

import java.util.*

class APIParameter private constructor() {
    private val parameter: APIParameter? = null
    val parameters = LinkedHashMap<String, String>()

    fun addParameter(key: String, value: String) {
        parameters[key] = value
    }

    fun removeParameter(key: String) {
        parameters.remove(key)
    }

    fun getParameter(key: String): String? {
        return if (parameters.containsKey(key)) {
            parameters[key]
        } else ""
    }

    fun getParameters(): Map<String, String> {
        return parameters
    }

    class Builder {
        private val buildParameter: APIParameter = APIParameter()

        fun build(): APIParameter {
            return buildParameter
        }
    }
}
