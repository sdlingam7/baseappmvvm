/*
 * Copyright 2019 PLAY DATE. All rights reserved.
 *
 * Dissemination of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from PLAY DATE.
 *
 * Created by Dharmalingam Sekar
 * Updated on 19/6/19 5:09 PM
 *
 */

package com.application.core.utils

import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

object Constants {
    object IntentResult {
        const val GALLERY = 1
        const val CAMERA = 2
        const val SCAN = 4
    }

    fun getCerts(): Array<TrustManager> {
        return arrayOf(object : X509TrustManager {

            override fun getAcceptedIssuers(): Array<X509Certificate> {
                return arrayOf()
            }


            @Throws(CertificateException::class)
            override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
            }

            @Throws(CertificateException::class)
            override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
            }
        })
    }

    fun getSSlContext(): SSLSocketFactory {
        // Install the all-trusting trust manager
        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, getCerts(), java.security.SecureRandom())
        // Create an ssl socket factory with our all-trusting manager
        return sslContext.socketFactory
    }
}

