/*
 * Copyright 2019 PLAY DATE. All rights reserved.
 *
 * Dissemination of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from PLAY DATE.
 *
 * Created by Dharmalingam Sekar
 * Updated on 19/6/19 5:09 PM
 *
 */

package com.application.core.utils.extension

import android.animation.ObjectAnimator
import android.graphics.Color
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import com.google.android.material.snackbar.Snackbar
import com.application.core.R

fun View.hide() {
    visibility = GONE
}

fun View.show() {
    visibility = VISIBLE
}

fun View.enable() {
    isEnabled = true
}

fun View.disable() {
    isEnabled = false
}

fun View.shakeView(duration: Long) {
    ObjectAnimator.ofFloat(this,
            View.TRANSLATION_X, 0f, 25f, -25f, 25f, -25f, 15f, -15f, 6f, -6f, 0f)
            .setDuration(duration)
            .start()
}

fun View.showSnackBar(message: String?) {
    val snackBar = message?.let {
        Snackbar
                .make(this, it, Snackbar.LENGTH_LONG)
    }
    snackBar?.view?.setBackgroundColor(context.getColorCode(R.color.colorRed))
    snackBar?.setActionTextColor(Color.WHITE)
    snackBar?.show()
}