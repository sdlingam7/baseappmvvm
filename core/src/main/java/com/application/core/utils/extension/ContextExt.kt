/*
 * Copyright 2019 PLAY DATE. All rights reserved.
 *
 * Dissemination of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from PLAY DATE.
 *
 * Created by Dharmalingam Sekar
 * Updated on 19/6/19 5:09 PM
 *
 */

package com.application.core.utils.extension

import android.content.Context
import android.content.res.ColorStateList
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat

fun Context.getColorCode(@ColorRes resourceId: Int) = ContextCompat.getColor(this, resourceId)

fun Context.getImageDrawable(@DrawableRes resourceId: Int) = ContextCompat.getDrawable(this, resourceId)

@Suppress("DEPRECATION")
fun Context.getColors(resourceId: Int): ColorStateList {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        resources.getColorStateList(resourceId, theme)
    else
        resources.getColorStateList(resourceId)
}

fun Context.isOnline(): Boolean {
    val connectivityManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val netInfo: NetworkInfo?
    netInfo = connectivityManager.activeNetworkInfo
    return netInfo != null && netInfo.isConnected
}



