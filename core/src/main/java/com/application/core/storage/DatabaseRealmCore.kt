/*
 * Copyright 2019 PLAY DATE. All rights reserved.
 *
 * Dissemination of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from PLAY DATE.
 *
 * Created by Dharmalingam Sekar
 * Updated on 19/6/19 5:09 PM
 *
 */

package com.application.core.storage

import android.content.Context
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmMigration
import io.realm.RealmObject
import io.realm.exceptions.RealmFileException
import io.realm.exceptions.RealmMigrationNeededException

open class DatabaseRealmCore {

    fun init(context: Context, encryptionKey: ByteArray, version: Long, migration: RealmMigration) {
        if (realmConfiguration == null) {
            //Init realm
            Realm.init(context)
            var localInstance: Realm? = null
            try {
                realmConfiguration = RealmConfiguration.Builder()
                        .encryptionKey(encryptionKey)
                        .schemaVersion(version)
                        .migration(migration)
                        .build()
                Realm.setDefaultConfiguration(realmConfiguration!!)
                localInstance = realmInstance
            } catch (rfe: RealmFileException) {
                rfe.printStackTrace()
            } catch (rfe: RealmMigrationNeededException) {
                rfe.printStackTrace()
            } finally {
                if (localInstance != null) {
                    if (!localInstance.isClosed) {
                        localInstance.close()
                    }
                }
            }
        } else {
            throw IllegalStateException("database already configured")
        }
    }

    fun <T : RealmObject> add(model: T) {
        realmInstance?.use { realm -> realm.executeTransaction { realm1 -> realm1.insertOrUpdate(model) } }
    }

    fun <T : RealmObject> addAll(model: List<T>) {
        realmInstance?.use { realm -> realm.executeTransaction { realm1 -> realm1.insertOrUpdate(model) } }
    }

    fun <T : RealmObject> findAll(clazz: Class<T>): List<T>? {
        return realmInstance?.where(clazz)?.findAll()
    }

    fun <T : RealmObject> findFirst(clazz: Class<T>): T? {
        return realmInstance?.where(clazz)?.findFirst()
    }

    fun <T : RealmObject> findLast(clazz: Class<T>): T? {
        return realmInstance?.where(clazz)?.findAll()?.last()
    }

    fun <T : RealmObject> delete(clazz: Class<T>) {
        realmInstance?.executeTransaction { realm -> realm.delete(clazz) }
    }

    fun close() {
        val realm = realmInstance
        realm?.close()
    }

    fun deleteAll() {
        val realm = realmInstance
        realm?.executeTransaction { realmDB -> realmDB.deleteAll() }
    }

    var realmConfiguration: RealmConfiguration? = null

    val realmInstance: Realm?
        get() = Realm.getDefaultInstance()

}