/*
 * Copyright 2019 PLAY DATE. All rights reserved.
 *
 * Dissemination of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from PLAY DATE.
 *
 * Created by Dharmalingam Sekar
 * Updated on 19/6/19 6:03 PM
 *
 */

package com.application.core.storage

import android.content.SharedPreferences
import java.util.*

abstract class PreferenceCore {

    protected abstract fun initPreference()

    fun write(key: String, value: String?) {
        val editor = prefs?.edit()
        editor?.putString(key, value)?.apply()
    }

    fun write(key: String, value: Int) {
        val editor = prefs?.edit()
        editor?.putInt(key, value)?.apply()
    }

    fun write(key: String, value: Float) {
        val editor = prefs?.edit()
        editor?.putFloat(key, value)?.apply()
    }

    fun write(key: String, value: Boolean) {
        val editor = prefs?.edit()
        editor?.putBoolean(key, value)?.apply()
    }

    fun getInt(key: String): Int {
        return if (prefs?.contains(key)!!) {
            prefs!!.getInt(key, -1)
        } else -1
    }

    fun getFloat(key: String): Float {
        return if (prefs?.contains(key)!!) {
            prefs!!.getFloat(key, 0f)
        } else 0f
    }

    fun getBoolean(key: String): Boolean {
        return if (prefs?.contains(key)!!) {
            prefs!!.getBoolean(key, false)
        } else false
    }

    fun getString(key: String): String? {
        return if (prefs?.contains(key)!!) {
            prefs?.getString(key, null)
        } else ""
    }




    var prefs: SharedPreferences? = null

    private fun generateTempToken(): String {
        return UUID.randomUUID().toString().replace("-".toRegex(), "") + UUID.randomUUID().toString().replace("-".toRegex(), "")
    }

    val encryptionKey: String?
        get() = getString(KEY_DB)

    fun setEncryptionKey() {
        if (!hasEncryptionKey()) {
            write(KEY_DB, generateTempToken())
        }
    }

    private fun hasEncryptionKey(): Boolean {
        return !getString(KEY_DB).isNullOrEmpty()
    }

    companion object {
        const val KEY_DB = "encryptionKeyDB"
    }
}