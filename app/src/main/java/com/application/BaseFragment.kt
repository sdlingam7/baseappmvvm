package com.application

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.base.mvvm.dagger.AppComponent

abstract class BaseFragment<VM : ViewModel, DB : ViewDataBinding>(private val mViewModelClass: Class<VM>) : Fragment() {
    open lateinit var mBinding: DB

    private var myApplication: MyApplication? = null

    val viewModel by lazy {
        ViewModelProviders.of(this).get(mViewModelClass)
    }

    private fun init(inflater: LayoutInflater, container: ViewGroup) {
        mBinding = DataBindingUtil.inflate(inflater, layoutId, container, false)
    }

    protected abstract val layoutId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        myApplication = activity?.applicationContext as MyApplication

        myApplication?.appComponent?.let {
            initViewModel(viewModel, it)
            injectComponent(it)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        container?.let { init(inflater, it) }
        return mBinding.root
    }

    protected abstract fun initViewModel(viewModel: VM, component: AppComponent)
    protected abstract fun injectComponent(component: AppComponent)

}