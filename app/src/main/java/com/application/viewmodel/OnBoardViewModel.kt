package com.application.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.base.mvvm.webservice.ApiServiceCall
import com.google.gson.Gson
import com.application.apiresponse.BaseResponse
import com.application.apiresponse.instagram.InstaUserResponse
import org.json.JSONObject
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class OnBoardViewModel : BaseViewModel() {

    private fun validateLogin(response: Any): BaseResponse {
        var result: BaseResponse = BaseResponse()
        var responseJSON = JSONObject(Gson().toJson(response))

        result.status = responseJSON.getString("status")
        result.message = responseJSON.getString("message")
        if (result.isSuccess) {
            appPreferenceManager.setAccessToken("Bearer " + responseJSON.getJSONObject("data").getString("accessToken"))
        }
        return result
    }

    fun getEmailLogin(strEmail: String, strPassword: String): MutableLiveData<BaseResponse> {
        var resultBaseResponse = MutableLiveData<BaseResponse>()
        ApiServiceCall.getEmailLogin(strEmail, strPassword, apiServiceInterface)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    resultBaseResponse.postValue(validateLogin(response))
                },
                { error ->
                    var error: BaseResponse = BaseResponse()
                    error.status = "error"
                    error.message = error.toString()
                    resultBaseResponse.postValue(error)
                    Log.e("Error", error.toString())
                }
            )
        return resultBaseResponse;
    }

    fun callInstagramUserInfo(accessToken: String): MutableLiveData<InstaUserResponse> {
        var resultInstaUser = MutableLiveData<InstaUserResponse>()
        ApiServiceCall.getInstaUserInfo(accessToken, instagramAPI)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    resultInstaUser.postValue(response)
                },
                { error ->
                    var error: InstaUserResponse =
                        InstaUserResponse()
                    error.success = "error"
                    error.message = error.toString()
                    resultInstaUser.postValue(error)
                    Log.e("Error", error.toString())
                }
            )
        return resultInstaUser;
    }


    fun getSocialLogin(strSource: String, strSourceId: String): MutableLiveData<BaseResponse> {
        var resultBaseResponse = MutableLiveData<BaseResponse>()
        ApiServiceCall.getSocialLogin(strSource, strSourceId, apiServiceInterface)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    resultBaseResponse.postValue(validateLogin(response))
                },
                { error ->
                    var error: BaseResponse = BaseResponse()
                    error.status = "error"
                    error.message = error.toString()
                    resultBaseResponse.postValue(error)
                    Log.e("Error", error.toString())
                }
            )
        return resultBaseResponse;
    }

    fun getRegister(
        strFirstName: String,
        strEmail: String,
        strPhoneNumber: String,
        strImage: String,
        strPassword: String,
        strSource: String,
        strSourceId: String
    ): MutableLiveData<BaseResponse> {
        var resultBaseResponse = MutableLiveData<BaseResponse>()
        ApiServiceCall.getRegister(
            strFirstName,
            strEmail,
            strPhoneNumber,
            strImage,
            strPassword,
            strSource,
            strSourceId,
            apiServiceInterface
        )
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    resultBaseResponse.postValue(validateLogin(response))
                },
                { error ->
                    var error: BaseResponse = BaseResponse()
                    error.status = "error"
                    error.message = error.toString()
                    resultBaseResponse.postValue(error)
                    Log.e("Error", error.toString())
                }
            )
        return resultBaseResponse;
    }

    fun getUpdateProfile(
        strFirstName: String,
        strLastName: String,
        strEmail: String,
        strPhone: String,
        strImage: String
    ): MutableLiveData<BaseResponse> {
        var resultBaseResponse = MutableLiveData<BaseResponse>()
        ApiServiceCall.getUpdateProfile(
            appPreferenceManager.getAccessToken(),
            strFirstName,
            strLastName,
            strEmail,
            strPhone,
            strImage,
            apiServiceInterface
        )
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    resultBaseResponse.postValue(response)
                },
                { error ->
                    var error: BaseResponse = BaseResponse()
                    error.status = "error"
                    error.message = error.toString()
                    resultBaseResponse.postValue(error)
                    Log.e("Error", error.toString())
                }
            )
        return resultBaseResponse;
    }

    fun getSentOTP(): MutableLiveData<BaseResponse> {
        var resultBaseResponse = MutableLiveData<BaseResponse>()
        ApiServiceCall.getSentOTP(appPreferenceManager.getAccessToken(), apiServiceInterface)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    resultBaseResponse.postValue(response)
                },
                { error ->
                    var error: BaseResponse = BaseResponse()
                    error.status = "error"
                    error.message = error.toString()
                    resultBaseResponse.postValue(error)
                    Log.e("Error", error.toString())
                }
            )
        return resultBaseResponse;
    }

    fun getVerifyOTP(strOTP: String): MutableLiveData<BaseResponse> {
        var resultBaseResponse = MutableLiveData<BaseResponse>()
        ApiServiceCall.getVerifydOTP(appPreferenceManager.getAccessToken(), strOTP, apiServiceInterface)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    resultBaseResponse.postValue(response)
                },
                { error ->
                    var error: BaseResponse = BaseResponse()
                    error.status = "error"
                    error.message = error.toString()
                    resultBaseResponse.postValue(error)
                    Log.e("Error", error.toString())
                }
            )
        return resultBaseResponse;
    }


}