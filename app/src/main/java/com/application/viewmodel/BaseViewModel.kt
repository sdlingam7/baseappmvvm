package com.application.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.toboa.api_response.ResultAppSetting
import app.toboa.api_response.ResultUserInfo
import com.base.mvvm.webservice.ApiServiceCall
import com.base.mvvm.webservice.ApiServiceInterface
import com.google.gson.Gson
import com.application.preference.AppPreferenceManager
import com.application.webservice.InstagramAPI
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

open class BaseViewModel : ViewModel() {

    @Inject
    lateinit var apiServiceInterface: ApiServiceInterface

    @Inject
    lateinit var appPreferenceManager: AppPreferenceManager

    @Inject
    lateinit var instagramAPI: InstagramAPI

    fun getAccessUserInfo(): MutableLiveData<ResultUserInfo> {
        var userInfo = MutableLiveData<ResultUserInfo>()
        ApiServiceCall.getAccessUserInfo(appPreferenceManager.getAccessToken(), apiServiceInterface)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    appPreferenceManager.setUserInfo(Gson().toJson(response.data))
                    userInfo.postValue(response)
                },
                {
                    var error: ResultUserInfo = ResultUserInfo()
                    error.status = "error"
                    error.message = error.toString()
                    userInfo.postValue(error)
                    Log.e("Error", error.toString())
                }
            )
        return userInfo;
    }

    fun getAccessAppSetting(): MutableLiveData<ResultAppSetting> {
        var appSetting = MutableLiveData<ResultAppSetting>()
        ApiServiceCall.getAccessAppSetting(apiServiceInterface)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    appPreferenceManager.setAppSetting(Gson().toJson(response.data))
                    appSetting.postValue(response)
                },
                {
                    var error: ResultAppSetting = ResultAppSetting()
                    error.status = "error"
                    error.message = error.toString()
                    appSetting.postValue(error)
                    Log.e("Error", error.toString())
                }
            )
        return appSetting;
    }


}