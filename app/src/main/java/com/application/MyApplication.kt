package com.application

import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.util.Base64
import android.util.Log
import androidx.multidex.MultiDexApplication
import com.base.mvvm.dagger.AppComponent
import com.base.mvvm.dagger.AppModule
import com.base.mvvm.dagger.DaggerAppComponent
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class MyApplication : MultiDexApplication() {
    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        initComponent()
        initFirebase()
        printHashKey()
    }

    private fun initComponent() {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()

        appComponent.inject(this)
    }

    private fun initFirebase() {
        //FirebaseApp.initializeApp(this)
    }


    fun printHashKey() {
        // Add code to print out the key hash
        try {
            //getting application package name, as defined in manifest
            val packageName = applicationContext.packageName
            val info: PackageInfo
            //Retriving package info
            info = packageManager.getPackageInfo(
                packageName,
                PackageManager.GET_SIGNATURES
            )

            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {

        } catch (e: NoSuchAlgorithmException) {

        }

    }
}