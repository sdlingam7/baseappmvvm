package com.application

import android.os.Bundle
import androidx.lifecycle.Observer
import com.base.mvvm.dagger.AppComponent
import com.application.core.utils.extension.startAppActivityClear
import com.application.core.base.CustomActionWindow
import com.application.core.base.CustomActionWindow.showSingleButtonWindow
import com.application.activity.onboard.LoginActivity
import com.application.playdate.R
import com.application.playdate.databinding.ActivityIndexBinding
import com.application.viewmodel.IndexViewModel

class IndexActivity :
    BaseActivity<IndexViewModel, ActivityIndexBinding>(IndexViewModel::class.java) {

    override fun getLayoutRes(): Int {
        return R.layout.activity_index
    }

    override fun initViewModel(viewModel: IndexViewModel, component: AppComponent) {
        component.inject(viewModel)
    }

    override fun injectComponent(component: AppComponent) {
        component.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        callAccessAppSetting()
    }

    private fun callAccessAppSetting() {
        viewModel.getAccessAppSetting()
            .observe(this, Observer { response ->
                if (response.isSuccess) {
                    validateAppSetting()
                } else {
                    showErrorPanel("Error", response.message.toString())
                }
            })
    }

    private fun callAccessUserInfo() {
        viewModel.getAccessUserInfo()
            .observe(this, Observer { response ->
                if (response.isSuccess) {
                    validateUserProfile()
                } else {
                    showErrorPanel("Error", response.message.toString())
                }
            })
    }

    private fun validateAppSetting() {
        if (!settingInfo?.isAppEnable!!) {
            showSingleButtonWindow(
                this,
                "Application is under development. Stay with us", "Ok",
                object : CustomActionWindow.OnActionResult {
                    override fun onClickYes() {
                        finish()
                    }

                    override fun onClickNo() {

                    }
                })
        } else {
            if (appPreferenceManager.getAccessToken() == "") {
                startAppActivityClear(LoginActivity::class.java)
            } else {
                callAccessUserInfo()
            }
        }
    }

}
