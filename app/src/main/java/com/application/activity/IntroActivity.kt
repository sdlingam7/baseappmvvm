package com.application.activity

import android.os.Bundle
import com.base.mvvm.dagger.AppComponent
import com.application.BaseActivity
import com.application.playdate.R
import com.application.playdate.databinding.ActivityIntroBinding
import com.application.viewmodel.IntroViewModel

class IntroActivity : BaseActivity<IntroViewModel, ActivityIntroBinding>(IntroViewModel::class.java) {

    override fun getLayoutRes(): Int {
        return R.layout.activity_intro
    }

    override fun initViewModel(viewModel: IntroViewModel, component: AppComponent) {
        component.inject(viewModel)
    }

    override fun injectComponent(component: AppComponent) {
        component.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
}