package com.application.activity

import android.os.Bundle
import com.base.mvvm.dagger.AppComponent
import com.application.BaseActivity
import com.application.playdate.R
import com.application.playdate.databinding.ActivityHomeBinding
import com.application.viewmodel.HomeViewModel

class HomeActivity : BaseActivity<HomeViewModel, ActivityHomeBinding>(HomeViewModel::class.java) {

    override fun getLayoutRes(): Int {
        return R.layout.activity_home
    }

    override fun initViewModel(viewModel: HomeViewModel, component: AppComponent) {
        component.inject(viewModel)
    }

    override fun injectComponent(component: AppComponent) {
        component.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
}
