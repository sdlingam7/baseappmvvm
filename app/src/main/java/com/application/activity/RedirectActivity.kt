package com.application.activity

import android.os.Bundle
import com.base.mvvm.dagger.AppComponent
import com.application.core.utils.extension.startAppActivityPutExtra
import com.application.BaseActivity
import com.application.playdate.R
import com.application.activity.onboard.LoginActivity
import com.application.activity.onboard.RegisterActivity
import com.application.playdate.databinding.ActivityEmailLoginBinding
import com.application.viewmodel.OnBoardViewModel

class RedirectActivity : BaseActivity<OnBoardViewModel, ActivityEmailLoginBinding>(
    OnBoardViewModel::class.java) {

    override fun getLayoutRes(): Int {
        return R.layout.activity_redirect
    }

    override fun initViewModel(viewModel: OnBoardViewModel, component: AppComponent) {
        component.inject(viewModel)
    }

    override fun injectComponent(component: AppComponent) {
        component.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val data = this.intent.data
        if (data != null && data.scheme == "http" && data.fragment != null) {
            if (data.fragment.contains("access_token")) {
                val accessToken = data.fragment!!.replaceFirst("access_token=".toRegex(), "")
                if (accessToken != null) {
                    val params = HashMap<String, String>()
                    params["accessToken"] = accessToken
                    if (appPreferenceManager.getRedirect().equals("login")) {
                        startAppActivityPutExtra(LoginActivity::class.java, params)
                    } else if (appPreferenceManager.getRedirect().equals("register")) {
                        startAppActivityPutExtra(RegisterActivity::class.java, params)
                    }
                } else {
                    // handleSignInResult(...);
                }
            }
        }
    }
}

