package com.application.activity

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.Spanned
import com.base.mvvm.dagger.AppComponent
import com.application.core.utils.extension.getExtraString
import com.application.core.utils.extension.hide
import com.application.core.utils.extension.show
import com.application.BaseActivity
import com.application.playdate.R
import com.application.playdate.databinding.ActivityIntroBinding
import com.application.viewmodel.BaseViewModel
import kotlinx.android.synthetic.main.activity_terms.*
import kotlinx.android.synthetic.main.app_bar_2.*

class TermsActivity : BaseActivity<BaseViewModel, ActivityIntroBinding>(BaseViewModel::class.java) {

    override fun getLayoutRes(): Int {
        return R.layout.activity_terms
    }

    override fun initViewModel(viewModel: BaseViewModel, component: AppComponent) {
        component.inject(viewModel)
    }

    override fun injectComponent(component: AppComponent) {
        component.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val title = this.getExtraString("title")
        val content = this.getExtraString("content")

        txtAppHeader.text = title
        imgAppBack.setOnClickListener {
            finish()
        }

        if (content.startsWith("http")) {
            webScreen.loadUrl(content)
            webScreen.show()
            txtContent.hide()
        } else {
            txtContent.text = content.toSpanned()
            webScreen.hide()
            txtContent.show()
        }
    }

    private fun String.toSpanned(): Spanned {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
        } else {
            @Suppress("DEPRECATION")
            return Html.fromHtml(this)
        }
    }
}
