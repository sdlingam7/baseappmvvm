package com.application.activity.onboard

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.base.mvvm.dagger.AppComponent
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.application.core.utils.extension.getExtraString
import com.application.core.utils.extension.startAppActivity
import com.application.core.utils.extension.startAppActivityPutExtra
import com.application.BaseActivity
import com.application.playdate.R
import com.application.activity.TermsActivity
import com.application.playdate.databinding.ActivityLoginBinding
import com.application.util.Consts.SIGN_IN_GOOGLE
import com.application.util.login
import com.application.viewmodel.OnBoardViewModel
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity<OnBoardViewModel, ActivityLoginBinding>(OnBoardViewModel::class.java),
    View.OnClickListener {

    override fun getLayoutRes(): Int {
        return R.layout.activity_login
    }

    override fun initViewModel(viewModel: OnBoardViewModel, component: AppComponent) {
        component.inject(viewModel)
    }

    override fun injectComponent(component: AppComponent) {
        component.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupFacebook()

        relativeSignInFacebook.setOnClickListener(this)
        relativeSignInGoogle.setOnClickListener(this)
        relativeSignInInstagram.setOnClickListener(this)
        txtSignInOther.setOnClickListener(this)
        txtSignup.setOnClickListener(this)
        txtSignInTrouble.setOnClickListener(this)
        txtTerms.setOnClickListener(this)
        txtPrivacy.setOnClickListener(this)

        val accessToken = this.getExtraString("accessToken")
        if (accessToken != "") {
            callInstagramUserInfo(accessToken)
        }
    }

    fun setupFacebook() {
        loginButton = findViewById<View>(R.id.login_button) as LoginButton
        loginButton.setReadPermissions(listOf("email"))
        callbackManager = CallbackManager.Factory.create()

        loginButton.registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {

                    val request = GraphRequest.newMeRequest(
                        loginResult.accessToken
                    ) { `object`, _ ->
                        try {
                            val strSourceId = `object`.getString("id")
                            val strName = `object`.getString("name")
                            var strEmail = strSourceId
                            if (`object`.has("email")) {
                                strEmail = `object`.getString("email")
                            }
                            val strImage = "https://graph.facebook.com/" + strSourceId + "/picture?type=large"


                            if (LoginManager.getInstance() != null) {
                                LoginManager.getInstance().logOut()
                            }
                            getSocialLogin(strName!!, strEmail, strImage, login.FACEBOOK, strSourceId)
                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (LoginManager.getInstance() != null) {
                                LoginManager.getInstance().logOut()
                            }
                        }
                    }
                    val parameters = Bundle()
                    parameters.putString("fields", "id,name,email")
                    request.parameters = parameters
                    request.executeAsync()
                }

                override fun onCancel() {
                    Log.e("Error", "Cancel")
                }

                override fun onError(exception: FacebookException) {

                }
            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SIGN_IN_GOOGLE) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            handleSignInResult(result)
        } else if (type == login.FACEBOOK) {
            callbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun handleSignInResult(result: GoogleSignInResult) {
        if (result.isSuccess) {
            val acct = result.signInAccount
            if (acct != null) {
                val strSourceId = acct.id
                val strName = acct.displayName
                val strEmail = acct.email
                val strImage = acct.photoUrl

                logoutGoogleApiClient()
                getSocialLogin(strName!!, strEmail!!, strImage.toString(), login.GOOGLE, strSourceId.toString())
            } else
                Toast.makeText(this, "Authentication failure", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show()
        }
    }


    override fun onClick(v: View?) {
        val params = HashMap<String, String>()
        if (v != null) {
            when {
                v.id == relativeSignInGoogle.id -> {
                    type = login.GOOGLE
                    val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
                    startActivityForResult(signInIntent, SIGN_IN_GOOGLE)
                }
                v.id == relativeSignInFacebook.id -> {
                    type = login.FACEBOOK
                    loginButton.performClick()
                }
                v.id == relativeSignInInstagram.id -> {
                    type = login.INSTAGRAM
                    signInWithInstagram()
                }
                v.id == txtSignInOther.id -> {
                    startAppActivity(EmailLoginActivity::class.java)
                }
                v.id == txtSignup.id -> {
                    startAppActivity(RegisterActivity::class.java)
                }
                v.id == txtSignInTrouble.id -> {
                }
                v.id == txtTerms.id -> {
                    params["title"] = "Terms of Service"
                    params["content"] = settingInfo?.terms_url!!
                    startAppActivityPutExtra(TermsActivity::class.java, params)
                }
                v.id == txtPrivacy.id -> {
                    params["title"] = "Privacy Policy"
                    params["content"] = settingInfo?.terms_url!!
                    startAppActivityPutExtra(TermsActivity::class.java, params)
                }
            }
        }
    }


    private fun getSocialLogin(
        strName: String,
        strEmail: String,
        strImage: String,
        strSource: String,
        strSourceId: String
    ) {
        showProgressDialog()
        viewModel.getSocialLogin(strSource, strSourceId)
            .observe(this, androidx.lifecycle.Observer { response ->
                dismissProgressDialog()
                if (response.isSuccess) {
                    callAccessUserInfo()
                } else {
                    callSocialRegister(strName, strEmail, strImage, strSource, strSourceId);
                }
            })
    }

    private fun callSocialRegister(
        strName: String,
        strEmail: String,
        strImage: String,
        strSource: String,
        strSourceId: String
    ) {
        showProgressDialog()
        viewModel.getRegister(strName, strEmail, "", strImage, "", strSource, strSourceId)
            .observe(this, androidx.lifecycle.Observer { response ->
                dismissProgressDialog()
                if (response.isSuccess) {
                    callAccessUserInfo()
                } else {
                    showErrorPanel("Error", response.message.toString()!!)
                }
            })

    }

    private fun callAccessUserInfo() {
        showProgressDialog()
        viewModel.getAccessUserInfo()
            .observe(this, androidx.lifecycle.Observer { response ->
                dismissProgressDialog()
                if (response.isSuccess) {
                    validateUserProfile()
                } else {
                    showErrorPanel("Error", response.message.toString())
                }
            })
    }

    private fun callInstagramUserInfo(accessToken: String) {
        showProgressDialog()
        viewModel.callInstagramUserInfo(accessToken)
            .observe(this, androidx.lifecycle.Observer { response ->
                dismissProgressDialog()
                if (response.success == "true") {
                    var strName = response.data?.fullName
                    var strEmail = response.data?.id
                    var strImage = response.data?.profilePicture
                    var strSourceId = response.data?.id
                    getSocialLogin(strName!!, strEmail!!, strImage.toString(), login.INSTAGRAM, strSourceId.toString())
                } else {
                    showErrorPanel("Error", response.message.toString()!!)
                }
            })
    }
}
