package com.application.activity.onboard

import android.os.Bundle
import com.base.mvvm.dagger.AppComponent
import com.application.BaseActivity
import com.application.playdate.R
import com.application.playdate.databinding.ActivityEmailLoginBinding
import com.application.viewmodel.OnBoardViewModel

class EmailLoginActivity : BaseActivity<OnBoardViewModel, ActivityEmailLoginBinding>(
    OnBoardViewModel::class.java) {

    override fun getLayoutRes(): Int {
        return R.layout.activity_email_login
    }

    override fun initViewModel(viewModel: OnBoardViewModel, component: AppComponent) {
        component.inject(viewModel)
    }

    override fun injectComponent(component: AppComponent) {
        component.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
}
