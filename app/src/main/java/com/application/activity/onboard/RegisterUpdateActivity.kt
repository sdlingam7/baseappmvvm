package com.application.activity.onboard

import android.os.Bundle
import com.base.mvvm.dagger.AppComponent
import com.application.BaseActivity
import com.application.playdate.R
import com.application.playdate.databinding.ActivityRegisterUpdateBinding
import com.application.viewmodel.OnBoardViewModel

class RegisterUpdateActivity : BaseActivity<OnBoardViewModel, ActivityRegisterUpdateBinding>(
    OnBoardViewModel::class.java) {

    override fun getLayoutRes(): Int {
        return R.layout.activity_register_update
    }

    override fun initViewModel(viewModel: OnBoardViewModel, component: AppComponent) {
        component.inject(viewModel)
    }

    override fun injectComponent(component: AppComponent) {
        component.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
}
