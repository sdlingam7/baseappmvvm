package com.base.mvvm.webservice

import app.toboa.api_response.ResultAppSetting
import app.toboa.api_response.ResultUserInfo
import com.application.apiresponse.BaseResponse
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Header
import retrofit2.http.POST
import rx.Observable

interface ApiServiceInterface {

    @POST("setting.json")
    fun getAccessAppSetting(): Observable<ResultAppSetting>

    @POST("userinfo.json")
    fun getAccessUserInfo(@Header("Authorization") Authorization: String): Observable<ResultUserInfo>

    @POST("login_success.json")
    @FormUrlEncoded
    fun getEmailLogin(
        @Field("email") email: String,
        @Field("password") password: String
    ): Observable<Any>

    @POST("login_success.json")
    @FormUrlEncoded
    fun getSocialLogin(
        @Field("source") source: String,
        @Field("source_id") source_id: String
    ): Observable<Any>

    @POST("login_success.json")
    @FormUrlEncoded
    fun getRegister(
        @Field("name") name: String, @Field("email") email: String, @Field("phone") phone: String,
        @Field("photo") photo: String, @Field("password") password: String,
        @Field("source") source: String, @Field("source_id") source_id: String
    ): Observable<Any>

    @POST("common_success.json")
    @FormUrlEncoded
    fun getUpdateProfile(
        @Header("Authorization") Authorization: String,
        @Field("name") name: String, @Field("email") email: String,
        @Field("phone") phone: String, @Field("photo") photo: String
    ): Observable<BaseResponse>

    @POST("common_success.json")
    fun getSentOTP(@Header("Authorization") Authorization: String): Observable<BaseResponse>

    @POST("common_success.json")
    @FormUrlEncoded
    fun getVerifyOTP(
        @Header("Authorization") Authorization: String,
        @Field("otp") otp: String
    ): Observable<BaseResponse>


}