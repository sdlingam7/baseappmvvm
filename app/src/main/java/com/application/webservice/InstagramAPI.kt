/*
 * Copyright 2019 PLAY DATE. All rights reserved.
 *
 * Dissemination of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from PLAY DATE.
 *
 * Created by Dharmalingam Sekar
 * Updated on 11/7/19 3:52 PM
 *
 */

package com.application.webservice

import com.application.apiresponse.instagram.InstaUserResponse
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

interface InstagramAPI {

    @GET("users/self/?")
    fun getInstaUserInfo(@Query("access_token") access_token: String): Observable<InstaUserResponse>

}