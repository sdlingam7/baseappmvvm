package com.base.mvvm.webservice

import app.toboa.api_response.ResultAppSetting
import app.toboa.api_response.ResultUserInfo
import com.application.apiresponse.BaseResponse
import com.application.apiresponse.instagram.InstaUserResponse
import com.application.webservice.InstagramAPI
import rx.Observable


object ApiServiceCall {

    fun getInstaUserInfo(accessToken: String, instagramAPI: InstagramAPI): Observable<InstaUserResponse> {
        return instagramAPI.getInstaUserInfo(accessToken)
    }

    fun getAccessAppSetting(apiServiceInterface: ApiServiceInterface): Observable<ResultAppSetting> {
        return apiServiceInterface.getAccessAppSetting()
    }

    fun getAccessUserInfo(accessToken: String, apiServiceInterface: ApiServiceInterface): Observable<ResultUserInfo> {
        return apiServiceInterface.getAccessUserInfo(accessToken)
    }

    fun getEmailLogin(
        strEmailAddress: String,
        strPassword: String,
        apiServiceInterface: ApiServiceInterface
    ): Observable<Any> {
        return apiServiceInterface.getEmailLogin(strEmailAddress, strPassword)
    }

    fun getSocialLogin(
        strSource: String,
        strSourceId: String,
        apiServiceInterface: ApiServiceInterface
    ): Observable<Any> {
        return apiServiceInterface.getSocialLogin(strSource, strSourceId)
    }

    fun getRegister(
        strFirstName: String, strEmail: String, strPhoneNumber: String,
        strImage: String, strPassword: String, strSource: String, strSourceId: String,
        apiServiceInterface: ApiServiceInterface
    ): Observable<Any> {
        return apiServiceInterface.getRegister(
            strFirstName, strEmail,
            strPhoneNumber, strImage, strPassword, strSource, strSourceId
        )
    }

    fun getUpdateProfile(
        accessToken: String, strFirstName: String, strLastName: String,
        strEmail: String, strPhone: String, strImage: String,
        apiServiceInterface: ApiServiceInterface
    ): Observable<BaseResponse> {
        return apiServiceInterface.getUpdateProfile(
            accessToken, strFirstName,
            strEmail, strPhone, strImage
        )
    }

    fun getSentOTP(accessToken: String, apiServiceInterface: ApiServiceInterface): Observable<BaseResponse> {
        return apiServiceInterface.getSentOTP(accessToken)
    }

    fun getVerifydOTP(
        accessToken: String,
        strOTP: String,
        apiServiceInterface: ApiServiceInterface
    ): Observable<BaseResponse> {
        return apiServiceInterface.getVerifyOTP(accessToken, strOTP)
    }

}