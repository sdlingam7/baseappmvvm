package com.application.api_response

import java.io.Serializable

open class ResAccessToken : Serializable {
    open
    var accessToken: String? = null
    var refreshToken: String? = null

}
