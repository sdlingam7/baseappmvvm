/*
 * Copyright 2019 PLAY DATE. All rights reserved.
 *
 * Dissemination of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from PLAY DATE.
 *
 * Created by Dharmalingam Sekar
 * Updated on 12/6/19 11:17 AM
 *
 */

package com.application.apiresponse

open class BaseResponse {
    var message: String? = null
    var status: String? = null

    open val isSuccess: Boolean
        get() = status == "true"
}
