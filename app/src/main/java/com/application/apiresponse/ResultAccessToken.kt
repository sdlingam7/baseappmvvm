package app.toboa.api_response

import com.application.api_response.ResAccessToken
import com.application.apiresponse.BaseResponse

class ResultAccessToken : BaseResponse() {
    var data: ResAccessToken? = null
}
