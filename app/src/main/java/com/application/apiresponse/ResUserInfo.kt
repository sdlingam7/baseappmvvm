package com.application.api_response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class ResUserInfo : Serializable {

    open
    var code: String? = null
    var name: String? = null
    var email: String? = null
    var mobile_no: String? = null
    var image: String? = null
    var avatar: String? = null
    var city: String? = null
    var country: String? = null
    var country_code: String? = null
    var password: String? = null
    var gender: String? = null

    @SerializedName("IsEmailVerify")
    var is_email_verified: String? = "0"
    @SerializedName("IsPhoneNoVerify")
    var is_phone_verified: String? = "0"

    @SerializedName("approves_count")
    var total_likes: String? = null

    @SerializedName("judges_count")
    var total_dislikes: String? = null

    @SerializedName("comments_count")
    var total_comments: String? = null

    @SerializedName("post_count")
    var total_confessions: String? = null
    var approved: String? = null
    var rejected: String? = null
    var pending: String? = null
    var category_ids: String? = null

    var notification: String? = null
    var confessionLike: String? = null
    var confessionDislike: String? = null
    var confessionComment: String? = null
    var commentLike: String? = null
    var commentDislike: String? = null
    var commentReply: String? = null
    var replyLike: String? = null
    var replyDislike: String? = null

    var expiryOn: String? = null
    var premiumDays: String? = "0"

}
