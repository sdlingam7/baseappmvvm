/*
 * Copyright 2019 PLAY DATE. All rights reserved.
 *
 * Dissemination of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from PLAY DATE.
 *
 * Created by Dharmalingam Sekar
 * Updated on 11/7/19 4:04 PM
 *
 */

package com.application.apiresponse.instagram

import com.google.gson.annotations.SerializedName

data class Counts(

	@field:SerializedName("followed_by")
	val followedBy: Int? = null,

	@field:SerializedName("follows")
	val follows: Int? = null,

	@field:SerializedName("media")
	val media: Int? = null
)