/*
 * Copyright 2019 PLAY DATE. All rights reserved.
 *
 * Dissemination of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from PLAY DATE.
 *
 * Created by Dharmalingam Sekar
 * Updated on 11/7/19 4:04 PM
 *
 */

package com.application.apiresponse.instagram

import com.google.gson.annotations.SerializedName

data class Data(

    @field:SerializedName("is_business")
    val isBusiness: Boolean? = null,

    @field:SerializedName("website")
    val website: String? = null,

    @field:SerializedName("full_name")
    val fullName: String? = null,

    @field:SerializedName("counts")
    val counts: Counts? = null,

    @field:SerializedName("bio")
    val bio: String? = null,

    @field:SerializedName("profile_picture")
    val profilePicture: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("username")
    val username: String? = null
)