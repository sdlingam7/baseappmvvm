package app.toboa.api_response

import com.application.api_response.ResUserInfo
import com.application.apiresponse.BaseResponse

class ResultUserInfo : BaseResponse() {
    var data: ResUserInfo? = null
}
