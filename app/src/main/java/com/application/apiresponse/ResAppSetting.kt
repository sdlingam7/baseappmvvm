package com.application.api_response

import java.io.Serializable

open class ResAppSetting : Serializable {
    open
    var terms_url: String? = null
    var privacy_url: String? = null
    var app_enable: String? = null
    var sms_enable: String? = null
    var default_ccp: String? = null
    var currency_symbol: String? = null

    open val isAppEnable: Boolean
        get() = app_enable == "true"

}
