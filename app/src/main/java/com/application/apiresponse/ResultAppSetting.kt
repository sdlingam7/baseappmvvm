package app.toboa.api_response

import com.application.api_response.ResAppSetting
import com.application.apiresponse.BaseResponse

class ResultAppSetting : BaseResponse() {
    var data: ResAppSetting? = null
}
