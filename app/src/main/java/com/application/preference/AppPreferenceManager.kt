/*
 * Copyright 2019 PLAY DATE. All rights reserved.
 *
 * Dissemination of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from PLAY DATE.
 *
 * Created by Dharmalingam Sekar
 * Updated on 19/6/19 6:46 PM
 *
 */

package com.application.preference

import android.content.Context
import com.application.core.storage.PreferenceCore
import com.google.gson.Gson
import com.securepreferences.SecurePreferences
import com.application.api_response.ResAppSetting
import com.application.api_response.ResUserInfo
import com.application.MyApplication
import com.application.playdate.R

class AppPreferenceManager : PreferenceCore {
    var context: Context? = null

    constructor(myApplication: MyApplication) {
        myApplication.appComponent.inject(this)
        context = myApplication.applicationContext
        initPreference()
    }

    constructor(context: Context) {
        this.context = context
        initPreference()
    }

    override fun initPreference() {
        prefs = SecurePreferences(
            context,
            context?.getString(R.string.preference_password),
            context?.getString(R.string.preference_name)
        )
    }

    fun setAccessToken(token: String) {
        write(KEY_USER_TOKEN, token)
    }

    fun getAccessToken(): String {
        return getString(KEY_USER_TOKEN).toString()
    }

    fun clearAccessToken() {
        write(KEY_USER_TOKEN, "")
    }

    fun setUserInfo(user: String) {
        write(KEY_USER_INFO, user)
    }

    fun getAccessUserInfo(): ResUserInfo? {
        if (getString(KEY_USER_INFO).toString().equals("")) {
            return null
        }
        return Gson().fromJson<ResUserInfo>(getString(KEY_USER_INFO).toString(), ResUserInfo::class.java)
    }

    fun clearUserInfo() {
        write(KEY_USER_INFO, "")
    }


    fun setAppSetting(setting: String) {
        write(KEY_APP_SETTING, setting)
    }

    fun getAccessAppSetting(): ResAppSetting? {
        if (getString(KEY_APP_SETTING).toString().equals("")) {
            return null
        }
        return Gson().fromJson<ResAppSetting>(getString(KEY_APP_SETTING).toString(), ResAppSetting::class.java)
    }

    fun clearAppSetting() {
        write(KEY_APP_SETTING, "")
    }

    fun setRedirect(page: String) {
        write(KEY_REDIRECT, page)
    }

    fun getRedirect(): String {
        return getString(KEY_REDIRECT).toString()
    }

    fun clearRedirect() {
        write(KEY_REDIRECT, "")
    }

    companion object {
        const val KEY_USER_TOKEN = "KEY_USER_TOKEN"
        const val KEY_USER_INFO = "KEY_USER_INFO"
        const val KEY_APP_SETTING = "KEY_APP_SETTING"
        const val KEY_REDIRECT = "KEY_REDIRECT"
    }
}
