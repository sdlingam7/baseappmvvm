package com.application

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.base.mvvm.dagger.AppComponent
import com.facebook.CallbackManager
import com.facebook.login.widget.LoginButton
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.application.api_response.ResAppSetting
import com.application.api_response.ResUserInfo
import com.application.playdate.R
import com.application.preference.AppPreferenceManager
import com.wang.avi.AVLoadingIndicatorView
import javax.inject.Inject

abstract class BaseActivity<VM : ViewModel, DB : ViewDataBinding>(private val mViewModelClass: Class<VM>) :
    AppCompatActivity(), GoogleApiClient.OnConnectionFailedListener {

    lateinit var callbackManager: CallbackManager
    lateinit var loginButton: LoginButton
    var mGoogleApiClient: GoogleApiClient? = null
    private val RC_SIGN_IN = 100
    internal var type = ""
    var strDeviceId = ""
    var strDeviceType = "Android"

    var userInfo: ResUserInfo? = null
    var settingInfo: ResAppSetting? = null

    @Inject
    lateinit var appPreferenceManager: AppPreferenceManager

    private var dialog: Dialog? = null

    val binding by lazy {
        DataBindingUtil.setContentView(this, getLayoutRes()) as DB
    }

    @LayoutRes
    abstract fun getLayoutRes(): Int

    val viewModel by lazy {
        ViewModelProviders.of(this).get(mViewModelClass)
    }

    private var myApplication: MyApplication? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(getLayoutRes())

        myApplication = applicationContext as MyApplication

        myApplication?.appComponent?.let { initViewModel(viewModel, it) }

        myApplication?.appComponent?.let { injectComponent(it) }

        setProgressDialog()
        accessUserData()
        accessAppSettingData()

    }

    private fun accessUserData() {
        userInfo = appPreferenceManager.getAccessUserInfo()
    }

    private fun accessAppSettingData() {
        settingInfo = appPreferenceManager.getAccessAppSetting()
    }

    private fun setProgressDialog() {
        dialog = Dialog(this)
        dialog?.setContentView(R.layout.loading_progress)
        dialog?.setCancelable(false)
        val indicator = intent.getStringExtra("indicator")
        val avi = dialog?.findViewById<AVLoadingIndicatorView>(R.id.avi) as AVLoadingIndicatorView
        avi.setIndicator(indicator)
        avi.show()
        if (dialog?.window != null) {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    fun showProgressDialog() {
        if (!isFinishing && dialog != null) {
            dialog?.show()
        }
    }

    fun dismissProgressDialog() {
        if (!isFinishing && dialog != null) {
            dialog?.dismiss()
        }
    }

    fun setGoogleApiClient() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build();

        mGoogleApiClient = GoogleApiClient.Builder(this)
            .enableAutoManage(this, this)
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .addOnConnectionFailedListener(this)
            .build()
    }

    fun logoutGoogleApiClient() {
        mGoogleApiClient!!.connect()
        mGoogleApiClient!!.registerConnectionCallbacks(object : GoogleApiClient.ConnectionCallbacks {
            override fun onConnected(bundle: Bundle?) {
                if (mGoogleApiClient!!.isConnected) {
                    Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback { }
                }
            }

            override fun onConnectionSuspended(i: Int) {

            }
        })
    }

    fun signInWithInstagram() {
        val uriBuilder = Uri.Builder()
        uriBuilder.scheme("https")
            .authority("api.instagram.com")
            .appendPath("oauth")
            .appendPath("authorize")
            .appendQueryParameter("client_id", this.resources.getString(R.string.instagram_client_id))
            .appendQueryParameter("redirect_uri", this.resources.getString(R.string.instagram_callback))
            .appendQueryParameter("response_type", "token")
        val browser = Intent(Intent.ACTION_VIEW, uriBuilder.build())
        startActivity(browser)
    }

    fun showErrorPanel(title: String, message: String) {
        try {
            val alertDialogBuilder = AlertDialog.Builder(this)
            alertDialogBuilder.setMessage(message)
            if (title.isNotEmpty())
                alertDialogBuilder.setTitle(title)
            alertDialogBuilder.setPositiveButton(
                "OK"
            ) { dialog, _ -> dialog.dismiss() }

            val alertDialog = alertDialogBuilder.create()
            alertDialog.show()
        } catch (e: Exception) {

        }
    }

    fun validateUserProfile() {
        //   startAppActivityClear(PersonalInfoActivity::class.java)
        val params = HashMap<String, String>()
        accessUserData()
//        if (!userInfo!!.email!!.isValidEmail() || userInfo!!.phone.equals("")) {
//            params["request"] = "update"
//            startAppActivityPutExtra(RegisterUpdateActivity::class.java, params)
//        } else if (userInfo!!.phone_verified.equals("0")) {
//            params["request"] = "otp"
//            startAppActivityPutExtra(RegisterUpdateActivity::class.java, params)
//        } else if (userInfo!!.gender == "" || userInfo!!.dob == "" || userInfo!!.ethinicity == null) {
//            startAppActivityClear(PersonalInfoActivity::class.java)
//        } else if (userInfo!!.faceshape == null || userInfo!!.hairtype == null ||
//            userInfo!!.eyeshape == null || userInfo!!.eyebrowshape == null ||
//            userInfo!!.skintone == null || userInfo!!.skintype == null ||
//            userInfo!!.nailshape == null
//        ) {
//            startAppActivityClear(BeautyProfileActivity::class.java)
//        } else {
//            startAppActivityClear(HomeActivity::class.java)
//        }
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    abstract fun initViewModel(viewModel: VM, component: AppComponent)
    protected abstract fun injectComponent(component: AppComponent)
}
