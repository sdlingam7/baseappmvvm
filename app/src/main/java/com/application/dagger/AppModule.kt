package com.base.mvvm.dagger

import com.base.mvvm.webservice.ApiServiceInterface
import com.application.core.exception.ConnectivityInterceptor
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.application.core.utils.Constants
import com.application.playdate.BuildConfig
import com.application.MyApplication
import com.application.preference.AppPreferenceManager
import com.application.webservice.InstagramAPI
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class AppModule(myApplication: MyApplication) {
    var myApplication: MyApplication = myApplication

    // Web service
    // Pref
    // Database

    @Provides
    @Singleton
    internal fun provideOkHttpCache(myApplication: MyApplication): Cache {
        val cacheSize = 10 * 1024 * 1024 // 10 MiB
        return Cache(myApplication.cacheDir, cacheSize.toLong())
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
        gsonBuilder.setLenient()
        gsonBuilder.setPrettyPrinting()
        gsonBuilder.setPrettyPrinting()
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
            .serializeNulls()
        return gsonBuilder.create()
    }

    companion object {
        private var logLevel =
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        val okHttpLogLevel = logLevel
    }

    @Provides
    @Singleton
    internal fun provideOkHttpClient(): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = okHttpLogLevel

        return OkHttpClient.Builder()
            .connectionSpecs(Arrays.asList(ConnectionSpec.MODERN_TLS, ConnectionSpec.CLEARTEXT))
            .sslSocketFactory(Constants.getSSlContext(), Constants.getCerts()[0] as javax.net.ssl.X509TrustManager)
            .hostnameVerifier { _, _ -> true }
            .addInterceptor(loggingInterceptor)
            .addInterceptor(ConnectivityInterceptor(myApplication.applicationContext))
            .readTimeout(180, TimeUnit.SECONDS)
            .connectTimeout(180, TimeUnit.SECONDS)
            .build()

    }


    @Provides
    internal fun provideAppAPI(gson: Gson, okHttpClient: OkHttpClient): ApiServiceInterface {
        var retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.SERVER_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .client(okHttpClient)
            .build()

        return retrofit.create(ApiServiceInterface::class.java)
    }

    @Provides
    internal fun provideInstaGramAPI(gson: Gson, okHttpClient: OkHttpClient): InstagramAPI {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.instagram.com/v1/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .client(okHttpClient)
            .build()

        return retrofit.create(InstagramAPI::class.java)
    }


    @Provides
    @Singleton
    fun provideAppPreferenceManager(): AppPreferenceManager {
        return AppPreferenceManager(myApplication)
    }
}