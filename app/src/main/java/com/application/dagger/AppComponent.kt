package com.base.mvvm.dagger

import com.application.IndexActivity
import com.application.MyApplication
import com.application.activity.HomeActivity
import com.application.activity.IntroActivity
import com.application.activity.RedirectActivity
import com.application.activity.TermsActivity
import com.application.activity.onboard.EmailLoginActivity
import com.application.activity.onboard.LoginActivity
import com.application.activity.onboard.RegisterActivity
import com.application.activity.onboard.RegisterUpdateActivity
import com.application.preference.AppPreferenceManager
import com.application.viewmodel.*
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class)])
interface AppComponent {

    fun inject(myApplication: MyApplication)
    fun inject(appPreferenceManager: AppPreferenceManager)

    //Activity Inject
    fun inject(indexActivity: IndexActivity)

    fun inject(homeActivity: HomeActivity)

    fun inject(viewModel: IndexViewModel)
    fun inject(viewModel: OnBoardViewModel)
    fun inject(loginActivity: LoginActivity)
    fun inject(emailLoginActivity: EmailLoginActivity)
    fun inject(registerActivity: RegisterActivity)
    fun inject(registerUpdateActivity: RegisterUpdateActivity)
    fun inject(viewModel: HomeViewModel)
    fun inject(viewModel: IntroViewModel)
    fun inject(introActivity: IntroActivity)
    fun inject(redirectActivity: RedirectActivity)
    fun inject(termsActivity: TermsActivity)
    fun inject(viewModel: BaseViewModel)

}