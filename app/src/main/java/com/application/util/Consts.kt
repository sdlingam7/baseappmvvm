/*
 * Copyright 2019 PLAY DATE. All rights reserved.
 *
 * Dissemination of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from PLAY DATE.
 *
 * Created by Dharmalingam Sekar
 * Updated on 4/7/19 3:48 PM
 *
 */

package com.application.util

object Consts {

    val SIGN_IN_GOOGLE = 100
}